FROM haproxy:latest
EXPOSE 443
EXPOSE 445
EXPOSE 450
COPY haproxy.cfg /usr/local/etc/haproxy/haproxy.cfg
run apt-get update
run cat /etc/apt/sources.list
run echo  "deb http://deb.torproject.org/torproject.org quantal main" >> /etc/apt/sources.list
run cat /etc/apt/sources.list
run apt-get install -y obfsproxy
ENV LOG_MIN_SEVERITY info
ENV DATA_DIR /var/lib/obfsproxy
ENV PASSWORD SBSB4444FANGBINXING4SBSBSBSBSBSB
ENV DEST_ADDR 127.0.0.10
ENV DEST_PORT 873
ENV RUN_MODE server
ENV LISTEN_ADDR 0.0.0.0
ENV LISTEN_PORT 443

CMD obfsproxy --data-dir ~/.obfs/ scramblesuit --dest $DEST_ADDR:$DEST_PORT --password $PASSWORD $RUN_MODE $LISTEN_ADDR:$LISTEN_PORT && haproxy -f /usr/local/etc/haproxy/haproxy.cfg